$(function () {
    // Registriert ein Klick-Event auf das HTML-Element mit der id="euklid".
    $("#euklid").click(function (event) {
        // Deklariert die Variable x und initialisiert
        // diese mit dem Wert aus dem HTML-Element mit der id="x".
        let x: number = Number($("#x").val());
        // Deklariert die Variable y und initialisiert
        // diese mit dem Wert aus dem HTML-Element mit der id="y".
        let y: number = Number($("#y").val());
        let t: number;

        // Berechnung des ggT
        while (x > 0) {
            if (x < y) {
                t = x;
                x = y;
                y = t;
            }
            x = x - y;
        }

        // Zeigt das Ergebnis der Berechnung im HTML-Element mit der id="ggtErgebnis" an.
        $("#ggtErgebnis").text("Der ggT von: " +
            $("#x").val() +
            " und " +
            $("#y").val() +
            " ist: " +
            y);
    });
});

// Aufgabe 1 Logische Operationen

$('#3bit').on('click', () => {
    if (<number>$('#zahl').val() > 3)
        $('#3bitErgebnis').html('Das dritte Bit ist gesetzt!')
    else
        $('#3bitErgebnis').html('Das dritte Bit ist NICHT gesetzt!')
})

// Aufgabe 2 Sortieren

$('#sort').on('click', () => {
    let a: number = parseInt($('#zahl1').val().toString())
    let b: number = parseInt($('#zahl2').val().toString())
    let c: number = parseInt($('#zahl3').val().toString())
    let min, max, mitte: number
    if (a>b) {
        if (a > c) {
            max = a;
            if (b > c) {
                mitte = b;
                min = c;
            }
            else {
                mitte = c;
                min = b;
            }
        }
        else {
            mitte = a;
            if (b > c) {
                max = b;
                min = c;
            }
            else {
                max = c;
                min = b;
            }
        }
    }
    else {
        if (b > c) {
            max = b;
            if (a > c) {
                mitte = a;
                min = c;
            }
            else {
                mitte = c;
                min = a;
            }
        }
        else {
            mitte = b;
            max = c;
            min = a;
        }
    }
    $('#sort_Ergebnis').html(min+"->"+mitte+"->"+max)

})

// Aufgabe 3 Benotung
$('#benotung').on('click', () => {
    let n: number = parseInt($('#note').val().toString())
    switch (n){
        case 1 :
            $('#benotung_Ergebnis').html('Ihre Bewertung: sehr gut')
            break
        case 2 :
            $('#benotung_Ergebnis').html('Ihre Bewertung: gut')
            break
        case 3 :
            $('#benotung_Ergebnis').html('Ihre Bewertung: befriedigend')
            break
        case 4 :
            $('#benotung_Ergebnis').html('Ihre Bewertung: ausreichend')
            break
        case 5 :
            $('#benotung_Ergebnis').html('Ihre Bewertung: mangelhaft')
            break
        case 6 :
            $('#benotung_Ergebnis').html('Ihre Bewertung: ungenügend')
            break
        default:
            $('#benotung_Ergebnis').html('')
            alert("Fehler! Ungültiger Wert.")

    }
})

// Aufgabe 4 Fakultät

$('#fakultät').on('click', () => {
    let n: number = parseInt($('#fakultät_Zahl').val().toString())
    let ergebnis : number = 1
    for(let i:number = 1 ;i<=n;i++){
        ergebnis=ergebnis*i;
    }
    $('#fakultät_Ergebnis').html(n+"!="+ergebnis)
})
// Aufgabe 5 Zinsberechnung

$('#zinsberechnung').on('click', () => {
    let startK: number = parseInt($('#startKapital').val().toString())
    let endK: number = parseInt($('#endKapital').val().toString())
    let ergebnis : number = 0
    while (startK < endK){
        startK= startK+(2/100)*startK
        ergebnis++
    }
    $('#zinsberechnung_Ergebnis').html("Anzahl der Jahre: "+ergebnis)
})